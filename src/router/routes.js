const routes = [
  {
    path: '/',
    component: () => import('@/layouts/index'),
    name: 'MainLayout',
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('../views/Dashboard/Default.vue')
      },
      {
        path: '/leads',
        name: 'Home',
        component: () => import('../views/Home.vue')
      },
      {
        path: '/billing',
        name: 'Billing',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Billing/Billing.vue')
      },
      {
        path: '/teachers',
        name: 'Teachers',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(
            /* webpackChunkName: "about" */ '../views/Teachers/Default.vue'
          )
      },
      {
        path: '/students',
        name: 'Students',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(
            /* webpackChunkName: "about" */ '../views/Students/Students.vue'
          )
      },
      {
        path: '/groups',
        name: 'Groups',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Groups/Groups.vue')
      },
      {
        path: '/group',
        name: 'Group',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Group/Default.vue')
      },
      {
        path: '/finance',
        name: 'Finance',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Finance/Finance.vue')
      },
      {
        path: '/debtors',
        name: 'Debtors',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Debtors/Debtors.vue')
      },
      {
        path: '/branches',
        name: 'Branches',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(
            /* webpackChunkName: "about" */ '../views/Branches/Branches.vue'
          )
      },
      {
        path: '/profile',
        name: 'Profile',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Profile/Profile.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'LoginLayout',
    component: () => import('@/layouts/Login'),
    children: [
      {
        path: '',
        name: 'Login',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/Login/Login.vue')
      },
      {
        path: '/DashboardLayout',
        name: 'DashboardLayout',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ '../views/DashboardLayout.vue')
      }
    ]
  },
  {
    path: '/form',
    name: 'LoginLayout',
    component: () => import('@/layouts/Form'),
    children: [
      {
        path: '',
        name: 'Lead-Form',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(
            /* webpackChunkName: "about" */ '../views/Leads/components/LeadForm.vue'
          )
      }
    ]
  }
];

export default routes;
