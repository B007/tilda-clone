import Vue from 'vue';
import App from './App.vue';
import store from './store';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router';
import locale from 'element-ui/lib/locale/lang/uz-UZ';
import i18n from './i18n';
import moment from 'moment';

Vue.use(ElementUI, {
  locale
});
Vue.config.productionTip = false;

new Vue({
  store,
  el: '#app',
  router,
  i18n,
  moment,
  render: (h) => h(App)
});
