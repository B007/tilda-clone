import Vue from 'vue'
import Vuex from 'vuex'
import leads from './modules/leads'
import teachers from './modules/teachers'
import students from './modules/students'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    leads,
    teachers,
    students,
  }
})
