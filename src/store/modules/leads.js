import axios from 'axios';

const state = {
  leads: []
};

const getters = {
  leads: (state) => state.leads,
  new_leads: (state) => state.leads.filter((e) => e.status === 'New'),
  ig_channel: (state) => state.leads.filter((e) => e.channel === 'Instagram'),
  fc_channel: (state) => state.leads.filter((e) => e.channel === 'Facebook'),
  ws_channel: (state) => state.leads.filter((e) => e.channel === 'Website')
};

const actions = {
  async fetchLeads({ commit }) {
    const response = await axios.get(`http://localhost:3000/New/`);

    commit('setLeads', response.data);
  },

  async addLead({ commit }, newLead) {
    const response = await axios.post(`http://localhost:3000/New/`, newLead);

    commit('addNewLead', response.data);
  },

  async deleteLead({ commit }, id) {
    await axios.delete(`http://localhost:3000/New/${id}`);
    // console.log(id);

    commit('removeLead', id);
  },

  async updateLead({ commit }, currentLead) {
    await axios.put(`http://localhost:3000/New/${currentLead.id}`, {
      ...currentLead
    });

    commit('renewLead', currentLead);
  }
};

const mutations = {
  setLeads: (state, leads) => (state.leads = leads),
  addNewLead: (state, lead) => state.leads.unshift(lead),
  removeLead: (state, id) =>
    (state.leads = state.leads.filter((lead) => lead.id !== id)),
  renewLead: (state, currentLead) => {
    const index = state.leads.findIndex((lead) => lead.id === currentLead.id);

    if (index !== -1) {
      state.leads.splice(index, 1, currentLead);
    }
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
