import axios from 'axios';

const state = {
  students: []
};

const getters = {
  students: (state) => state.students
};

const actions = {
  async fetchStudents({ commit }) {
    const response = await axios.get(`http://localhost:3000/students`);

    commit('setStudents', response.data);
  },

  async createStudent({ commit }, newStudent) {
    const response = await axios.post(
      `http://localhost:3000/students`,
      newStudent
    );

    commit('addNewStudent', response.data);
  }
};

const mutations = {
  setStudents: (state, students) => (state.students = students),
  addNewStudent: (state, student) => state.students.unshift(student)
};

export default {
  state,
  getters,
  actions,
  mutations
};
