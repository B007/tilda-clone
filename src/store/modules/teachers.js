import axios from 'axios';

const state = {
    teachers: []
}

const getters = {
    teachers: state => state.teachers
};

const actions = {
    async fetchTeachers({ commit }){
        const response = await axios.get(`http://localhost:3000/teachers`)

        commit('setTeachers', response.data);
    }
};

const mutations = {
    setTeachers: (state, teachers) => (state.teachers = teachers),
};

export default {
    state,
    getters,
    actions,
    mutations,
}