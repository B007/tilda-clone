# Data Box

This is a platform to manage:

- Leads
- Students & Debtors
- Finance
- Staff Members
- Multiple Branches

---

## Demo

URL: https://data-box.netlify.app/

---

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/B007/tilda-clone.git
```

Go to the project directory

```bash
  cd data-box
```

Install dependencies

```bash
  npm install
  npm install element-ui
```

Start the server

```bash
  npm run serve
  json-server --watch db.json
```

---

## Tech Stack

**Client:** Vue, Javascript, Element UI

**Server:** JSON server

---

## Author

- [@B007](https://gitlab.com/B007) (Gitlab Account)

---

## Feedback

If you have any feedback, please reach out to us at BekhruzSHofficial@gmail.com

---

### Run Json-Sever

```
json-server --watch db.json
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

---

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
